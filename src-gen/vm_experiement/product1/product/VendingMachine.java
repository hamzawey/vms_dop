package vm_experiement.product1.product;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import vm_experiement.product1.org.lsmr.vending.frontend.Coin;
import vm_experiement.product1.org.lsmr.vending.frontend.Deliverable;
import vm_experiement.product1.org.lsmr.vending.frontend.Pop;
/*** added by dVendingMachine
 */
@SuppressWarnings("unchecked")
public class VendingMachine {
	private ArrayList<Deliverable> deliveryChute = new ArrayList<Deliverable>();
	private int valueOfEnteredCoins = 0;
	private List<Coin> individualCoinsEntered = new ArrayList<Coin>();
	private List<Coin> paymentCoins = new ArrayList<Coin>();
	private int [] coinKinds;
	private Map<Integer, Integer> valueToIndexMap = new TreeMap<Integer,
		Integer>();
	private Map<Integer, Object> coins;
	private String [] popKindNames;
	private int [] popKindCosts;
	private Map<Integer, Object> pops;
	public VendingMachine(List<Integer> coinKinds, int buttonCount) {
		if(coinKinds == null || coinKinds.size() < 1) throw new
		IllegalArgumentException("There must be at least one coin kind");
		if(buttonCount < 1) throw new
		IllegalArgumentException("There must be at least one selection button");
		this.coinKinds = new int[coinKinds.size()];
		coins = new HashMap<Integer, Object>();
		int i = 0;
		for(Integer ck : coinKinds) {
			Integer index = valueToIndexMap.get(ck);
			if(index != null) throw new
			IllegalStateException("Coin kinds must have unique denominations");
			if(ck <= 0) throw new
			IllegalArgumentException("Each coin kind must be postive");
			valueToIndexMap.put(ck, i);
			this.coinKinds[i ++] = ck.intValue();
		}
		popKindNames = new String[buttonCount];
		for(int j = 0;
			j < popKindNames.length;
			j ++) popKindNames[j] = "";
		popKindCosts = new int[buttonCount];
		pops = new HashMap<Integer, Object>();
	}
	public void configure(List<String> names, List<Integer> costs) {
		if(names == null || names.size() != popKindNames.length) throw new
		IllegalArgumentException("The names list must be of identical size as the number of selection buttons");
		if(costs == null || costs.size() != popKindCosts.length) throw new
		IllegalArgumentException("The costs list must be of identical size as the number of selection buttons");
		int i = 0;
		for(String name : names) if(name.equals("\"\"")) throw new
		IllegalArgumentException("Names cannot have zero length");
		i = 0;
		for(int cost : costs) if(cost < 1) throw new
		IllegalArgumentException("Costs must be > 0");
		i = 0;
		for(String name : names) popKindNames[i ++] = name;
		i = 0;
		for(int cost : costs) popKindCosts[i ++] = cost;
	}
	public void loadCoins(int coinKindIndex, Coin [] coins) {
		if(coins == null) throw new
		IllegalArgumentException("The list cannot be null");
		if(coinKindIndex < 0 || coinKindIndex >= this.coinKinds.length) throw new
		IllegalArgumentException("The coin kind index must be >= 0 and < number of coin kinds");
		ArrayList<Coin> coinList = ( ArrayList<Coin> )
		this.coins.get(coinKindIndex);
		if(coinList == null) {
			coinList = new ArrayList<Coin>();
			this.coins.put(coinKindIndex, coinList);
		}
		for(Coin coin : coins) coinList.add(coin);
	}
	public void loadPops(int popKindIndex, Pop [] pops) {
		if(pops == null) throw new
		IllegalArgumentException("The list cannot be empty");
		if(popKindIndex < 0 || popKindIndex >= this.popKindNames.length) throw new
		IllegalArgumentException("The pop kind index must be >= 0 and < number of coin kinds");
		ArrayList<Pop> popList = ( ArrayList<Pop> ) this.pops.get(popKindIndex);
		if(popList == null) {
			popList = new ArrayList<Pop>();
			this.pops.put(popKindIndex, popList);
		}
		for(Pop pop : pops) popList.add(pop);
	}
	public void press(int selectionButtonIndex) {
		if(selectionButtonIndex < 0 || selectionButtonIndex >= popKindNames.length)
		throw new
		IllegalArgumentException("The button number must be between 0 and "
			+(popKindNames.length - 1));
		if(popKindCosts[selectionButtonIndex] <= valueOfEnteredCoins) {
			ArrayList<Pop> popList = ( ArrayList<Pop> ) pops.get(selectionButtonIndex);
			if(popList != null && ! popList.isEmpty()) {
				deliveryChute.add(popList.remove(0));
				valueOfEnteredCoins = deliverChange(popKindCosts[selectionButtonIndex],
					valueOfEnteredCoins);
				paymentCoins.addAll(individualCoinsEntered);
				individualCoinsEntered.clear();
			}
			else {
			}
		}
		else {
		}
	}
	private Map<Integer, Object> changeHelper(ArrayList<Integer> values, int
		index, int changeDue) {
		if(index >= values.size()) return null;
		int value = values.get(index);
		Integer ck = valueToIndexMap.get(value);
		ArrayList<Coin> coinList = ( ArrayList<Coin> ) coins.get(ck);
		Map<Integer, Object> map = changeHelper(values, index + 1, changeDue);
		if(map == null) {
			map = new TreeMap<Integer, Object>(new Comparator<Integer>() {
					@Override
					public int compare(Integer o1, Integer o2) {
						return o2 - o1;
					}
				});
			map.put(0, new ArrayList<Integer>());
		}
		Map<Integer, Object> newMap = new TreeMap<Integer, Object>(map);
		for(Integer total : map.keySet()) {
			List<Integer> res = ( List<Integer> ) map.get(total);
			int intTotal = total;
			for(int count = coinList.size();
				count >= 0;
				count --) {
				int newTotal = count * value + intTotal;
				if(newTotal <= changeDue) {
					List<Integer> newRes = new ArrayList<Integer>();
					if(res != null) newRes.addAll(res);
					for(int i = 0;
						i < count;
						i ++) newRes.add(ck);
					newMap.put(newTotal, newRes);
				}
			}
		}
		return newMap;
	}
	private int deliverChange(int cost, int entered) {
		int changeDue = entered - cost;
		if(changeDue < 0) throw new
		InternalError("Cost was greater than entered, which should not happen");
		ArrayList<Integer> values = new ArrayList<Integer>();
		for(Integer ck : valueToIndexMap.keySet()) values.add(ck);
		Map<Integer, Object> map = changeHelper(values, 0, changeDue);
		List<Integer> res = ( List<Integer> ) map.get(changeDue);
		if(res == null) {
			Iterator<Integer> iter = map.keySet().iterator();
			Integer max = 0;
			while(iter.hasNext()) {
				Integer temp = iter.next();
				if(temp > max) max = temp;
			}
			res = ( List<Integer> ) map.get(max);
		}
		for(Integer ck : res) {
			List<Coin> coinList = ( List<Coin> ) coins.get(ck);
			Coin c = coinList.remove(0);
			deliveryChute.add(c);
			changeDue -= coinKinds[ck];
		}
		return changeDue;
	}
	public void insert(Coin coin) {
		if(coin == null) throw new IllegalArgumentException();
		int value = coin.getValue();
		if(valueToIndexMap.get(value) == null) deliveryChute.add(coin);
		else {
			individualCoinsEntered.add(coin);
			valueOfEnteredCoins += coin.getValue();
		}
	}
	public List<Object> unload() {
		ArrayList<Object> list = new ArrayList<Object>();
		ArrayList<Object> unusedCoins = new ArrayList<Object>();
		ArrayList<Object> paymentCoins = new ArrayList<Object>();
		ArrayList<Object> unsoldPop = new ArrayList<Object>();
		list.add(unusedCoins);
		list.add(paymentCoins);
		list.add(unsoldPop);
		for(Integer ck : this.coins.keySet()) {
			ArrayList<Coin> coins = ( ArrayList<Coin> ) this.coins.get(ck);
			unusedCoins.addAll(coins);
			coins.clear();
		}
		list.add(unusedCoins);
		paymentCoins.addAll(this.paymentCoins);
		this.paymentCoins.clear();
		paymentCoins.addAll(this.individualCoinsEntered);
		this.individualCoinsEntered.clear();
		for(Integer pk : this.pops.keySet()) {
			ArrayList<Pop> pops = ( ArrayList<Pop> ) this.pops.get(pk);
			unsoldPop.addAll(pops);
			pops.clear();
		}
		return list;
	}
	public List<Deliverable> extract() {
		ArrayList<Deliverable> res = new ArrayList<Deliverable>();
		res.addAll(deliveryChute);
		deliveryChute.clear();
		return res;
	}
}