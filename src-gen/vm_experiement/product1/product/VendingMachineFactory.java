package vm_experiement.product1.product;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Collections;
import vm_experiement.product1.org.lsmr.vending.frontend.Coin;
import vm_experiement.product1.org.lsmr.vending.frontend.Deliverable;
import
vm_experiement.product1.org.lsmr.vending.frontend.IVendingMachineFactory;
import vm_experiement.product1.org.lsmr.vending.frontend.Pop;
import vm_experiement.product1.org.lsmr.vending.frontend.ScriptProcessor;
import
vm_experiement.product1.org.lsmr.vending.frontend.parser.ParseException;
/*** added by dVendingMachineFactory
 */
public class VendingMachineFactory implements IVendingMachineFactory {
	public static void main(String [] args) throws ParseException, IOException {
		String location = "Product1_Scripts\\";
		String [] goodScripts = {
			"T01-good-insert-and-press-exact-change",
			"T02-good-insert-and-press-change-expected",
			"T03-good-teardown-without-configure-or-load",
			"T04-good-press-without-insert", "T05-good-scrambled-coin-kinds",
			"T06-good-extract-before-sale", "T07-good-changing-configuration",
			"T08-good-approximate-change", "T09-good-hard-for-change",
			"T10-good-invalid-coin", "T11-good-extract-before-sale-complex",
			"T12-good-approximate-change-with-credit"
		};
		String [] badScripts = {
			"U01-bad-configure-before-construct", "U02-bad-costs-list",
			"U03-bad-names-list", "U04-bad-non-unique-denomination",
			"U05-bad-coin-kind", "U06-bad-button-number", "U07-bad-button-number-2",
			"U08-bad-button-number-3"
		};
		check(goodScripts, badScripts, location);
	}
	private List<VendingMachine> vmList = new ArrayList<VendingMachine>();
	public static void check(String [] goodScripts, String [] badScripts, String
		location) {
		int count = 0, pass = 0, fail = 0;
		for(String script : goodScripts) try {
			count ++;
			new ScriptProcessor(location + script, new VendingMachineFactory(), true);
			pass ++;
		}
		catch(Exception t) {
			if(t instanceof RuntimeException || t instanceof ParseException) {
				fail ++;
				t.printStackTrace();
				System.err.println();
			}
		}
		for(String script : badScripts) try {
			count ++;
			new ScriptProcessor(location + script, new VendingMachineFactory(), true);
			fail ++;
		}
		catch(Exception t) {
			if(t instanceof ParseException || t instanceof RuntimeException) {
				pass ++;
				t.printStackTrace();
				System.err.println();
			}
		}
		System.err.println(count + " scripts executed: " + pass + " passed, " + pass
			+ " failed.");
	}
	public VendingMachineFactory() {
	}
	@Override
	public List<Deliverable> extractFromDeliveryChute(int vmIndex) {
		return vmList.get(vmIndex).extract();
	}
	@Override
	public void insertCoin(int vmIndex, Coin coin) {
		vmList.get(vmIndex).insert(coin);
	}
	@Override
	public void pressButton(int vmIndex, int selectionButtonIndex) {
		vmList.get(vmIndex).press(selectionButtonIndex);
	}
	@Override
	public int constructNewVendingMachine(List<Integer> coinKinds, int
		selectionButtonCount) {
		VendingMachine vm = new VendingMachine(coinKinds, selectionButtonCount);
		int index = vmList.size();
		vmList.add(vm);
		return index;
	}
	@Override
	public void configureVendingMachine(int vmIndex, List<String> popNames,
		List<Integer> popCosts) {
		vmList.get(vmIndex).configure(popNames, popCosts);
	}
	@Override
	public void loadCoins(int vmIndex, int coinKindIndex, Coin [] coins) {
		vmList.get(vmIndex).loadCoins(coinKindIndex, coins);
	}
	@Override
	public void loadPops(int vmIndex, int popKindIndex, Pop [] pops) {
		vmList.get(vmIndex).loadPops(popKindIndex, pops);
	}
	@Override
	public List<Object> unloadVendingMachine(int vmIndex) {
		return Arrays.<Object> asList(new ArrayList<Object>(new Integer(0)), new
			ArrayList<Object>(new Integer(0)), new ArrayList<Object>());
	}
}