package vm_experiement.product1.org.lsmr.vending.frontend;

import java.util.List;
/*** added by dIVendingMachineFactory
 */
public interface IVendingMachineFactory {
	public int constructNewVendingMachine(List<Integer> coinKinds, int
		selectionButtonCount);
	public void configureVendingMachine(int vmIndex, List<String> popNames,
		List<Integer> popCosts);
	public void loadCoins(int vmIndex, int coinKindIndex, Coin [] coins);
	public void loadPops(int vmIndex, int popKindIndex, Pop [] pops);
	public List<Object> unloadVendingMachine(int vmIndex);
	public List<Deliverable> extractFromDeliveryChute(int vmIndex);
	public void insertCoin(int vmIndex, Coin coin);
	public void pressButton(int vmIndex, int value);
}