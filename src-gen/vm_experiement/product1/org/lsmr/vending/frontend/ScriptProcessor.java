package vm_experiement.product1.org.lsmr.vending.frontend;

import java.io.FileReader;
import java.io.IOException;
import
vm_experiement.product1.org.lsmr.vending.frontend.parser.ParseException;
import vm_experiement.product1.org.lsmr.vending.frontend.parser.Parser;
/*** added by dScriptProcessor
 */
public class ScriptProcessor {
	public ScriptProcessor(String path, IVendingMachineFactory factory, boolean
		debug) throws IOException, ParseException {
		Parser p = new Parser(new FileReader(path));
		p.register(factory);
		p.setDebug(debug);
		p.process(path);
	}
}