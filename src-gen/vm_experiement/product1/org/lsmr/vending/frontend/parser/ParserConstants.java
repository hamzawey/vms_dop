package vm_experiement.product1.org.lsmr.vending.frontend.parser;

/*** added by dParserConstants
 */
@SuppressWarnings("all")
public interface ParserConstants {
	int EOF = 0;
	int SINGLE_LINE_COMMENT = 8;
	int CONSTRUCT = 11;
	int CONFIGURE = 12;
	int EXTRACT = 13;
	int PRESS = 14;
	int INSERT = 15;
	int COIN_LOAD = 16;
	int POP_LOAD = 17;
	int UNLOAD = 18;
	int CHECK_DELIVERY = 19;
	int CHECK_TEARDOWN = 20;
	int INTEGER_LITERAL = 21;
	int STRING_LITERAL = 22;
	int LPAREN = 23;
	int RPAREN = 24;
	int COMMA = 25;
	int SEMICOLON = 26;
	int DEFAULT = 0;
	int MULTI_LINE_COMMENT = 1;
	int FORMAL_COMMENT = 2;
	String [] tokenImage = {
		"<EOF>", "\" \"", "\"\\t\"", "\"\\n\"", "\"\\r\"", "\"\\f\"",
		"<token of kind 6>", "\"/*\"", "<SINGLE_LINE_COMMENT>", "\"*/\"",
		"<token of kind 10>", "\"construct\"", "\"configure\"", "\"extract\"",
		"\"press\"", "\"insert\"", "\"coin-load\"", "\"pop-load\"", "\"unload\"",
		"\"CHECK_DELIVERY\"", "\"CHECK_TEARDOWN\"", "<INTEGER_LITERAL>",
		"<STRING_LITERAL>", "\"(\"", "\")\"", "\",\"", "\";\"", "\"[\"", "\"]\""
	};
}