package vm_experiement.product2.org.lsmr.vending.frontend.hardware;

/*** added by dDisplay
 */
public final class Display extends AbstractHardware<DisplayListener> {
	private String message = null;
	public void display(String message) {
		String oldMessage = message;
		this.message = message;
		notifyMessageChange(oldMessage, this.message);
	}
	private void notifyMessageChange(String oldMessage, String newMessage) {
		for(DisplayListener listener : listeners) listener.messageChange(this,
			oldMessage, newMessage);
	}
}