package vm_experiement.product2.org.lsmr.vending.frontend.hardware;

import java.util.ArrayList;
import vm_experiement.product2.org.lsmr.vending.frontend.Coin;
import vm_experiement.product2.org.lsmr.vending.frontend.Deliverable;
import vm_experiement.product2.org.lsmr.vending.frontend.PopCan;
/*** added by dDeliveryChute
 */
public final class DeliveryChute extends
AbstractHardware<DeliveryChuteListener> implements AbstractCoinAcceptor,
AbstractPopCanAcceptor {
	private ArrayList<Deliverable> chute = new ArrayList<Deliverable>();
	private int maxCapacity;
	public DeliveryChute(int capacity) {
		if(capacity <= 0) throw new
		SimulationException("Capacity must be a positive value: " + capacity);
		this.maxCapacity = capacity;
	}
	public int size() {
		return chute.size();
	}
	public int getCapacity() {
		return maxCapacity;
	}
	@Override
	public void acceptPopCan(PopCan popCan) throws CapacityExceededException,
	DisabledException {
		if(isDisabled()) throw new DisabledException();
		if(chute.size() >= maxCapacity) throw new CapacityExceededException();
		chute.add(popCan);
		notifyItemDelivered();
		if(chute.size() >= maxCapacity) notifyChuteFull();
	}
	@Override
	public void acceptCoin(Coin coin) throws CapacityExceededException,
	DisabledException {
		if(isDisabled()) throw new DisabledException();
		if(chute.size() >= maxCapacity) throw new CapacityExceededException();
		chute.add(coin);
		notifyItemDelivered();
		if(chute.size() >= maxCapacity) notifyChuteFull();
	}
	public Deliverable [] removeItems() {
		notifyDoorOpened();
		Deliverable [] items = new Deliverable[chute.size()];
		chute.toArray(items);
		chute.clear();
		notifyDoorClosed();
		return items;
	}
	@Override
	public boolean hasSpace() {
		return chute.size() < maxCapacity;
	}
	private void notifyItemDelivered() {
		for(DeliveryChuteListener listener : listeners)
		listener.itemDelivered(this);
	}
	private void notifyDoorOpened() {
		for(DeliveryChuteListener listener : listeners) listener.doorOpened(this);
	}
	private void notifyDoorClosed() {
		for(DeliveryChuteListener listener : listeners) listener.doorClosed(this);
	}
	private void notifyChuteFull() {
		for(DeliveryChuteListener listener : listeners) listener.chuteFull(this);
	}
}