package vm_experiement.product2.org.lsmr.vending.frontend.hardware;

/*** added by dAbstractHardwareListener
 */
public interface AbstractHardwareListener {
	public void enabled(AbstractHardware<? extends AbstractHardwareListener>
		hardware);
	public void disabled(AbstractHardware<? extends AbstractHardwareListener>
		hardware);
}