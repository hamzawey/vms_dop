package vm_experiement.product2.org.lsmr.vending.frontend.hardware;

/*** added by dSimulationException
 */
@SuppressWarnings("serial")
public class SimulationException extends RuntimeException {
	private String n;
	public SimulationException(Exception n) {
		this.n = n.toString();
	}
	public SimulationException(String message) {
		n = message;
	}
	@Override
	public String toString() {
		return "Nested exception: " + n;
	}
}