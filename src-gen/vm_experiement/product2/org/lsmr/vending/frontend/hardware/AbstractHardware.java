package vm_experiement.product2.org.lsmr.vending.frontend.hardware;

import java.util.ArrayList;
/*** added by dAbstractHardware
 */
public abstract class AbstractHardware<T extends AbstractHardwareListener> {
	protected ArrayList<T> listeners = new ArrayList<T>();
	public final boolean deregister(T listener) {
		return listeners.remove(listener);
	}
	public final void deregisterAll() {
		listeners.clear();
	}
	public final void register(T listener) {
		listeners.add(listener);
	}
	private boolean disabled = false;
	public final void disable() {
		disabled = true;
		notifyDisabled();
	}
	private void notifyDisabled() {
		for(T listener : listeners) listener.disabled(this);
	}
	public final void enable() {
		disabled = false;
		notifyEnabled();
	}
	private void notifyEnabled() {
		for(T listener : listeners) listener.enabled(this);
	}
	public final boolean isDisabled() {
		return disabled;
	}
}