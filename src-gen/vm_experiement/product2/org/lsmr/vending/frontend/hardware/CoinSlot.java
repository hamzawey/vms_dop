package vm_experiement.product2.org.lsmr.vending.frontend.hardware;

import vm_experiement.product2.org.lsmr.vending.frontend.Coin;
/*** added by dCoinSlot
 */
public final class CoinSlot extends AbstractHardware<CoinSlotListener> {
	private int [] validValues;
	private CoinChannel valid, invalid;
	public CoinSlot(int [] validValues) {
		this.validValues = validValues;
	}
	public void connect(CoinChannel valid, CoinChannel invalid) {
		this.valid = valid;
		this.invalid = invalid;
	}
	private boolean isValid(Coin coin) {
		for(int vv : validValues) {
			if(vv == coin.getValue()) return true;
		}
		return false;
	}
	public void addCoin(Coin coin) throws DisabledException {
		if(isDisabled()) throw new DisabledException();
		if(isValid(coin) && valid.hasSpace()) {
			try {
				valid.deliver(coin);
			}
			catch(CapacityExceededException e) {
				throw new SimulationException(e);
			}
			finally {
				notifyValidCoinInserted(coin);
			}
		}
		else if(invalid.hasSpace()) {
			try {
				invalid.deliver(coin);
			}
			catch(CapacityExceededException e) {
				throw new SimulationException(e);
			}
			finally {
				notifyCoinRejected(coin);
			}
		}
		else throw new
		SimulationException("Unable to route coin: All channels full");
	}
	private void notifyValidCoinInserted(Coin coin) {
		for(CoinSlotListener listener : listeners) listener.validCoinInserted(this,
			coin);
	}
	private void notifyCoinRejected(Coin coin) {
		for(CoinSlotListener listener : listeners) listener.coinRejected(this,
			coin);
	}
}