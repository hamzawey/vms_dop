package vm_experiement.product2.org.lsmr.vending.frontend.hardware;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import vm_experiement.product2.org.lsmr.vending.frontend.Coin;
/*** added by dCoinReceptacle
 */
public final class CoinReceptacle extends
AbstractHardware<CoinReceptacleListener> implements AbstractCoinAcceptor {
	private Vector<Coin> coinsEntered = new Vector<Coin>();
	private int maxCapacity;
	private CoinChannel coinReturn, other = null;
	private HashMap<Integer, CoinChannel> coinRacks = new HashMap<Integer,
		CoinChannel>();
	public CoinReceptacle(int capacity) {
		if(capacity <= 0) throw new
		SimulationException("Capacity must be positive: " + capacity);
		maxCapacity = capacity;
	}
	public int getCapacity() {
		return maxCapacity;
	}
	public int size() {
		return coinsEntered.size();
	}
	public void connect(Map<Integer, CoinChannel> rackChannels, CoinChannel
		coinReturn, CoinChannel other) {
		if(rackChannels == null) this.coinRacks.clear();
		else this.coinRacks.putAll(rackChannels);
		this.coinReturn = coinReturn;
		this.other = other;
	}
	public void load(Coin [] coins) throws SimulationException {
		if(maxCapacity < coinsEntered.size() + coins.length) throw new
		SimulationException("Capacity exceeded by attempt to load");
		for(Coin coin : coins) coinsEntered.add(coin);
		notifyLoad(coins);
	}
	private void notifyLoad(Coin [] coins) {
		for(CoinReceptacleListener listener : listeners) listener.coinsLoaded(this,
			coins);
	}
	public List<Coin> unload() {
		List<Coin> result = new ArrayList<Coin>(coinsEntered);
		coinsEntered.clear();
		notifyUnload(result.toArray(new Coin[result.size()]));
		return result;
	}
	private void notifyUnload(Coin [] coins) {
		for(CoinReceptacleListener listener : listeners)
		listener.coinsUnloaded(this, coins);
	}
	public void acceptCoin(Coin coin) throws CapacityExceededException,
	DisabledException {
		if(isDisabled()) throw new DisabledException();
		if(coinsEntered.size() >= maxCapacity) throw new
		CapacityExceededException();
		coinsEntered.add(coin);
		notifyCoinAdded(coin);
		if(coinsEntered.size() >= maxCapacity) notifyCoinsFull();
	}
	public void storeCoins() throws CapacityExceededException, DisabledException
	{
		if(isDisabled()) throw new DisabledException();
		for(Coin coin : coinsEntered) {
			CoinChannel ccs = coinRacks.get(new Integer(coin.getValue()));
			if(ccs != null && ccs.hasSpace()) ccs.deliver(coin);
			else if(other != null) {
				if(other.hasSpace()) other.deliver(coin);
				else throw new CapacityExceededException();
			}
			else throw new
			SimulationException("The 'other' output channel has not been defined, but it is needed for storage.");
		}
		if(! coinsEntered.isEmpty()) {
			coinsEntered.clear();
			notifyCoinsRemoved();
		}
	}
	public void returnCoins() throws CapacityExceededException, DisabledException
	{
		if(isDisabled()) throw new DisabledException();
		for(Coin coin : coinsEntered) coinReturn.deliver(coin);
		if(! coinsEntered.isEmpty()) {
			coinsEntered.clear();
			notifyCoinsRemoved();
		}
	}
	@Override
	public boolean hasSpace() {
		return coinsEntered.size() < maxCapacity;
	}
	private void notifyCoinAdded(Coin coin) {
		for(CoinReceptacleListener listener : listeners) listener.coinAdded(this,
			coin);
	}
	private void notifyCoinsRemoved() {
		for(CoinReceptacleListener listener : listeners)
		listener.coinsRemoved(this);
	}
	private void notifyCoinsFull() {
		for(CoinReceptacleListener listener : listeners) listener.coinsFull(this);
	}
}