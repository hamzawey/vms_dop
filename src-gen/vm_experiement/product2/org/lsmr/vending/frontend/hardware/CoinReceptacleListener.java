package vm_experiement.product2.org.lsmr.vending.frontend.hardware;

import vm_experiement.product2.org.lsmr.vending.frontend.Coin;
/*** added by dCoinReceptacleListener
 */
public interface CoinReceptacleListener extends AbstractHardwareListener {
	void coinAdded(CoinReceptacle receptacle, Coin coin);
	void coinsRemoved(CoinReceptacle receptacle);
	void coinsFull(CoinReceptacle receptacle);
	void coinsLoaded(CoinReceptacle receptacle, Coin [] coins);
	void coinsUnloaded(CoinReceptacle receptacle, Coin [] coins);
}