package vm_experiement.product2.org.lsmr.vending.frontend.hardware;

import vm_experiement.product2.org.lsmr.vending.frontend.Coin;
/*** added by dCoinSlotListener
 */
public interface CoinSlotListener extends AbstractHardwareListener {
	void validCoinInserted(CoinSlot slot, Coin coin);
	void coinRejected(CoinSlot slot, Coin coin);
}