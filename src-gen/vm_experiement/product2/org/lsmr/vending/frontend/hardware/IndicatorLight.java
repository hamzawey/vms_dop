package vm_experiement.product2.org.lsmr.vending.frontend.hardware;

/*** added by dIndicatorLight
 */
public final class IndicatorLight extends
AbstractHardware<IndicatorLightListener> {
	private boolean on = false;
	public void activate() {
		on = true;
		notifyActivated();
	}
	public void deactivate() {
		on = false;
		notifyDeactivated();
	}
	public boolean isActive() {
		return on;
	}
	private void notifyActivated() {
		for(IndicatorLightListener listener : listeners) listener.activated(this);
	}
	private void notifyDeactivated() {
		for(IndicatorLightListener listener : listeners) listener.deactivated(this);
	}
}