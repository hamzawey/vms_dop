package vm_experiement.product2.org.lsmr.vending.frontend.hardware;

/*** added by dIndicatorLightListener
 */
public interface IndicatorLightListener extends AbstractHardwareListener {
	void activated(IndicatorLight light);
	void deactivated(IndicatorLight light);
}