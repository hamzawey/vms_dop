package vm_experiement.product2.org.lsmr.vending.frontend.hardware;

/*** added by dLockListener
 */
public interface LockListener extends AbstractHardwareListener {
	void locked(Lock lock);
	void unlocked(Lock lock);
}