package vm_experiement.product2.org.lsmr.vending.frontend.hardware;

import vm_experiement.product2.org.lsmr.vending.frontend.PopCan;
/*** added by dPopCanChannel
 */
public final class PopCanChannel implements AbstractPopCanAcceptor {
	private AbstractPopCanAcceptor sink;
	public PopCanChannel(AbstractPopCanAcceptor sink) {
		this.sink = sink;
	}
	@Override
	public void acceptPopCan(PopCan popCan) throws CapacityExceededException,
	DisabledException {
		sink.acceptPopCan(popCan);
	}
}