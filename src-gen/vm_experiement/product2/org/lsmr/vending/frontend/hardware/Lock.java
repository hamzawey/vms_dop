package vm_experiement.product2.org.lsmr.vending.frontend.hardware;

/*** added by dLock
 */
public final class Lock extends AbstractHardware<LockListener> {
	private boolean locked = true;
	public void lock() {
		locked = true;
		notifyLocked();
	}
	public void unlock() {
		locked = false;
		notifyUnlocked();
	}
	public boolean isLocked() {
		return locked;
	}
	private void notifyLocked() {
		for(LockListener listener : listeners) listener.locked(this);
	}
	private void notifyUnlocked() {
		for(LockListener listener : listeners) listener.unlocked(this);
	}
}