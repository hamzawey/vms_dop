package vm_experiement.product2.org.lsmr.vending.frontend.hardware;

/*** added by dDisplayListener
 */
public interface DisplayListener extends AbstractHardwareListener {
	void messageChange(Display display, String oldMessage, String newMessage);
}