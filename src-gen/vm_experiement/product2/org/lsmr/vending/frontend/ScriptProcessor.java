package vm_experiement.product2.org.lsmr.vending.frontend;

import java.io.FileReader;
import java.io.IOException;
import
vm_experiement.product2.org.lsmr.vending.frontend.parser.ParseException;
import vm_experiement.product2.org.lsmr.vending.frontend.parser.Parser;
import
vm_experiement.product2.org.lsmr.vending.frontend.hardware.DisabledException;
/*** added by dScriptProcessor* modified by dScriptProcessor_P2
 */
public class ScriptProcessor {
	/*** added by dScriptProcessor_P2
	 */
	public ScriptProcessor(String path, IVendingMachineFactory factory, boolean
		debug) throws IOException, ParseException, DisabledException {
		Parser p = new Parser(new FileReader(path));
		p.register(factory);
		p.setDebug(debug);
		p.process(path);
	}
}