package vm_experiement.product2.org.lsmr.vending.frontend;

import java.util.List;
import
vm_experiement.product2.org.lsmr.vending.frontend.hardware.DisabledException;
/*** added by dIVendingMachineFactory* modified by dIVendingMachineFactory_P2
 */
public interface IVendingMachineFactory {
	public void configureVendingMachine(int vmIndex, List<String> popNames,
		List<Integer> popCosts);
	public void loadCoins(int vmIndex, int coinKindIndex, Coin [] coins);
	public List<Deliverable> extractFromDeliveryChute(int vmIndex);
	public void pressButton(int vmIndex, int value);
	/*** added by dIVendingMachineFactory_P2
	 */
	public int constructNewVendingMachine(List<Integer> coinKinds, int
		selectionButtonCount, int coinRackCapacity, int popCanRackCapacity, int
		receptacleCapacity);
	/*** added by dIVendingMachineFactory_P2
	 */
	public void loadPopCans(int vmIndex, int popCanRackIndex, PopCan [] popCans);
	/*** added by dIVendingMachineFactory_P2
	 */
	public VendingMachineStoredContents unloadVendingMachine(int vmIndex);
	/*** added by dIVendingMachineFactory_P2
	 */
	public void insertCoin(int vmIndex, Coin coin) throws DisabledException;
}