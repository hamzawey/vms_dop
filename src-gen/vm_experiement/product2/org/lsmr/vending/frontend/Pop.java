package vm_experiement.product2.org.lsmr.vending.frontend;

/*** added by dPop
 */
public class Pop implements Deliverable {
	private String name;
	public Pop(String name) {
		if(name == null || name.length() == 0) throw new
		IllegalArgumentException("The argument cannot be null or an empty string");
		this.name = name;
	}
	public String getName() {
		return name;
	}
	@Override
	public String toString() {
		return getName();
	}
}