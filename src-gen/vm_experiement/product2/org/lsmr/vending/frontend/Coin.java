package vm_experiement.product2.org.lsmr.vending.frontend;

/*** added by dCoin
 */
public class Coin implements Deliverable {
	private int value;
	public Coin(int value) {
		if(value <= 0) throw new
		IllegalArgumentException("The value must be greater than 0: the argument passed was "
			+ value);
		this.value = value;
	}
	public int getValue() {
		return value;
	}
	@Override
	public String toString() {
		return "" + getValue();
	}
}