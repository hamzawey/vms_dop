package vm_experiement.product3.product;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Collections;
import vm_experiement.product3.org.lsmr.vending.frontend.Coin;
import vm_experiement.product3.org.lsmr.vending.frontend.Deliverable;
import
vm_experiement.product3.org.lsmr.vending.frontend.IVendingMachineFactory;
import vm_experiement.product3.org.lsmr.vending.frontend.ScriptProcessor;
import
vm_experiement.product3.org.lsmr.vending.frontend.parser.ParseException;
import vm_experiement.product3.org.lsmr.vending.frontend.Coin;
import vm_experiement.product3.org.lsmr.vending.frontend.Deliverable;
import
vm_experiement.product3.org.lsmr.vending.frontend.IVendingMachineFactory;
import vm_experiement.product3.org.lsmr.vending.frontend.PopCan;
import vm_experiement.product3.org.lsmr.vending.frontend.ScriptProcessor;
import
vm_experiement.product3.org.lsmr.vending.frontend.VendingMachineStoredContents;
import vm_experiement.product3.org.lsmr.vending.frontend.hardware.CoinRack;
import
vm_experiement.product3.org.lsmr.vending.frontend.hardware.DisabledException;
import vm_experiement.product3.org.lsmr.vending.frontend.hardware.PopCanRack;
import
vm_experiement.product3.org.lsmr.vending.frontend.hardware.VendingMachine;
import
vm_experiement.product3.org.lsmr.vending.frontend.parser.ParseException;
/*** added by dVendingMachineFactory* modified by dVendingMachineFactory_P2
 */
public class VendingMachineFactory implements IVendingMachineFactory {
	private List<VendingMachine> vmList = new ArrayList<VendingMachine>();
	public static void check(String [] goodScripts, String [] badScripts, String
		location) {
		int count = 0, pass = 0, fail = 0;
		for(String script : goodScripts) try {
			count ++;
			new ScriptProcessor(location + script, new VendingMachineFactory(), true);
			pass ++;
		}
		catch(Exception t) {
			if(t instanceof RuntimeException || t instanceof ParseException) {
				fail ++;
				t.printStackTrace();
				System.err.println();
			}
		}
		for(String script : badScripts) try {
			count ++;
			new ScriptProcessor(location + script, new VendingMachineFactory(), true);
			fail ++;
		}
		catch(Exception t) {
			if(t instanceof ParseException || t instanceof RuntimeException) {
				pass ++;
				t.printStackTrace();
				System.err.println();
			}
		}
		System.err.println(count + " scripts executed: " + pass + " passed, " + pass
			+ " failed.");
	}
	public VendingMachineFactory() {
	}
	/*** added by dVendingMachineFactory_P2
	 */
	public static void main(String [] args) throws ParseException, IOException {
		String location = "Product2_Scripts\\";
		String [] goodScripts = {
			"T01-good-insert-and-press-exact-change",
			"T02-good-insert-and-press-change-expected",
			"T03-good-teardown-without-configure-or-load",
			"T04-good-press-without-insert", "T05-good-scrambled-coin-kinds",
			"T06-good-extract-before-sale", "T07-good-changing-configuration",
			"T08-good-approximate-change", "T09-good-hard-for-change",
			"T10-good-invalid-coin", "T11-good-extract-before-sale-complex",
			"T12-good-approximate-change-with-credit", "T13-good-need-to-store-payment"
		};
		String [] badScripts = {
			"U01-bad-configure-before-construct", "U02-bad-costs-list",
			"U03-bad-names-list", "U04-bad-non-unique-denomination",
			"U05-bad-coin-kind", "U06-bad-button-number", "U07-bad-button-number-2",
			"U08-bad-button-number-3"
		};
		check(goodScripts, badScripts, location);
	}
	/*** added by dVendingMachineFactory_P2
	 */
	@Override
	public List<Deliverable> extractFromDeliveryChute(int vmIndex) {
		return Arrays.asList(vmList.get(vmIndex).getDeliveryChute().removeItems());
	}
	/*** added by dVendingMachineFactory_P2
	 */
	@Override
	public void insertCoin(int vmIndex, Coin coin) throws DisabledException {
		vmList.get(vmIndex).getCoinSlot().addCoin(coin);
	}
	/*** added by dVendingMachineFactory_P2
	 */
	@Override
	public void pressButton(int vmIndex, int value) {
		vmList.get(vmIndex).getSelectionButton(value).press();
	}
	/*** added by dVendingMachineFactory_P2
	 */
	@Override
	public int constructNewVendingMachine(List<Integer> coinKinds, int
		selectionButtonCount, int coinRackCapacity, int popCanRackCapacity, int
		receptacleCapacity) {
		int [] ck = new int[coinKinds.size()];
		int i = 0;
		for(Integer coinKind : coinKinds) ck[i ++] = coinKind;
		VendingMachine vm = new VendingMachine(ck, selectionButtonCount,
			coinRackCapacity, popCanRackCapacity, receptacleCapacity);
		new VendingMachineLogic(vm);
		vmList.add(vm);
		return vmList.size() - 1;
	}
	/*** added by dVendingMachineFactory_P2
	 */
	@Override
	public void configureVendingMachine(int vmIndex, List<String> popNames,
		List<Integer> popCosts) {
		VendingMachine vm = vmList.get(vmIndex);
		if(popNames == null || popNames.size() != vm.getNumberOfPopCanRacks()) throw
		new
		IllegalArgumentException("The names list must be of identical size as the number of selection buttons");
		if(popCosts == null || popCosts.size() != vm.getNumberOfPopCanRacks()) throw
		new
		IllegalArgumentException("The costs list must be of identical size as the number of selection buttons");
		for(String name : popNames) if(name.equals("\"\"")) throw new
		IllegalArgumentException("Names cannot have zero length");
		for(int cost : popCosts) if(cost < 1) throw new
		IllegalArgumentException("Costs must be > 0");
		vm.configure(popNames, popCosts);
	}
	/*** added by dVendingMachineFactory_P2
	 */
	@Override
	public void loadPopCans(int vmIndex, int popCanRackIndex, PopCan [] popCans)
	{
		VendingMachine vm = vmList.get(vmIndex);
		PopCanRack pcr = vm.getPopCanRack(popCanRackIndex);
		pcr.load(popCans);
	}
	/*** added by dVendingMachineFactory_P2
	 */
	@Override
	public void loadCoins(int vmIndex, int coinRackIndex, Coin [] coins) {
		VendingMachine vm = vmList.get(vmIndex);
		CoinRack cr = vm.getCoinRack(coinRackIndex);
		cr.load(coins);
	}
	/*** added by dVendingMachineFactory_P2
	 */
	@Override
	public VendingMachineStoredContents unloadVendingMachine(int vmIndex) {
		VendingMachine vm = vmList.get(vmIndex);
		VendingMachineStoredContents contents = new VendingMachineStoredContents();
		contents.paymentCoinsInStorageBin.addAll(vm.getStorageBin().unload());
		for(int i = 0;
			i < vm.getNumberOfPopCanRacks();
			i ++) {
			PopCanRack pcr = vm.getPopCanRack(i);
			contents.unsoldPopCans.add(new ArrayList<PopCan>(pcr.unload()));
		}
		for(int i = 0;
			i < vm.getNumberOfCoinRacks();
			i ++) {
			CoinRack cr = vm.getCoinRack(i);
			contents.unusedCoinsForChange.add(new ArrayList<Coin>(cr.unload()));
		}
		return contents;
	}
}