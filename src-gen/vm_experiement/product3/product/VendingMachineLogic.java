package vm_experiement.product3.product;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;
import vm_experiement.product3.org.lsmr.vending.frontend.Coin;
import
vm_experiement.product3.org.lsmr.vending.frontend.hardware.AbstractHardware;
import
vm_experiement.product3.org.lsmr.vending.frontend.hardware.AbstractHardwareListener;
import
vm_experiement.product3.org.lsmr.vending.frontend.hardware.CapacityExceededException;
import vm_experiement.product3.org.lsmr.vending.frontend.hardware.CoinRack;
import vm_experiement.product3.org.lsmr.vending.frontend.hardware.CoinSlot;
import
vm_experiement.product3.org.lsmr.vending.frontend.hardware.CoinSlotListener;
import
vm_experiement.product3.org.lsmr.vending.frontend.hardware.DisabledException;
import vm_experiement.product3.org.lsmr.vending.frontend.hardware.Display;
import
vm_experiement.product3.org.lsmr.vending.frontend.hardware.EmptyException;
import vm_experiement.product3.org.lsmr.vending.frontend.hardware.PopCanRack;
import
vm_experiement.product3.org.lsmr.vending.frontend.hardware.SelectionButton;
import
vm_experiement.product3.org.lsmr.vending.frontend.hardware.SelectionButtonListener;
import
vm_experiement.product3.org.lsmr.vending.frontend.hardware.SimulationException;
import
vm_experiement.product3.org.lsmr.vending.frontend.hardware.VendingMachine;
import vm_experiement.product3.org.lsmr.vending.frontend.Cents;
/*** added by dVendingMachineLogic_P2* modified by dVendingMachineLogic3
 */
public class VendingMachineLogic implements CoinSlotListener,
SelectionButtonListener {
	private int availableFunds = 0;
	private VendingMachine vendingMachine;
	private Map<SelectionButton, Integer> buttonToIndex = new
	HashMap<SelectionButton, Integer>();
	private Map<Integer, Integer> valueToIndexMap = new HashMap<Integer,
		Integer>();
	public VendingMachineLogic(VendingMachine vm) {
		vendingMachine = vm;
		vm.getCoinSlot().register(this);
		for(int i = 0;
			i < vm.getNumberOfSelectionButtons();
			i ++) {
			SelectionButton sb = vm.getSelectionButton(i);
			sb.register(this);
			buttonToIndex.put(sb, i);
		}
		for(int i = 0;
			i < vm.getNumberOfCoinRacks();
			i ++) {
			int value = vm.getCoinKindForCoinRack(i);
			valueToIndexMap.put(value, i);
		}
	}
	@Override
	public void enabled(AbstractHardware<? extends AbstractHardwareListener>
		hardware) {
	}
	@Override
	public void disabled(AbstractHardware<? extends AbstractHardwareListener>
		hardware) {
	}
	@Override
	public void coinRejected(CoinSlot coinSlotSimulator, Coin coin) {
	}
	@Override
	public void pressed(SelectionButton button) {
		Integer index = buttonToIndex.get(button);
		if(index == null) throw new
		SimulationException("An invalid selection button was pressed");
		int cost = vendingMachine.getPopKindCost(index);
		if(cost <= availableFunds) {
			PopCanRack pcr = vendingMachine.getPopCanRack(index);
			if(pcr.size() > 0) {
				try {
					pcr.dispensePopCan();
					vendingMachine.getCoinReceptacle().storeCoins();
					availableFunds = deliverChange(cost, availableFunds);
				}
				catch(Exception e) {
					if(e instanceof DisabledException || e instanceof EmptyException || e
						instanceof CapacityExceededException) {
						throw new SimulationException(e);
					}
				}
			}
		}
		else {
			Display disp = vendingMachine.getDisplay();
			disp.display("Cost: " + cost + "; available funds: " + availableFunds);
			final Timer timer = new Timer();
			timer.schedule(new TimerTask() {
					@Override
					public void run() {
						timer.cancel();
					}
				}, 5000);
		}
	}
	private Map<Integer, Object> changeHelper(ArrayList<Integer> values, int
		index, int changeDue) {
		if(index >= values.size()) return null;
		int value = values.get(index);
		Integer ck = valueToIndexMap.get(value);
		CoinRack cr = vendingMachine.getCoinRack(ck);
		Map<Integer, Object> map = changeHelper(values, index + 1, changeDue);
		if(map == null) {
			map = new TreeMap<Integer, Object>(new Comparator<Integer>() {
					@Override
					public int compare(Integer o1, Integer o2) {
						return o2 - o1;
					}
				});
			map.put(0, new ArrayList<Integer>());
		}
		Map<Integer, Object> newMap = new TreeMap<Integer, Object>(map);
		for(Integer total : map.keySet()) {
			List<Integer> res = ( List<Integer> ) map.get(total);
			for(int count = cr.size();
				count >= 0;
				count --) {
				int newTotal =(count * value) + total;
				if(newTotal <= changeDue) {
					List<Integer> newRes = new ArrayList<Integer>();
					if(res != null) newRes.addAll(res);
					for(int i = 0;
						i < count;
						i ++) newRes.add(ck);
					newMap.put(newTotal, newRes);
				}
			}
		}
		return newMap;
	}
	private int deliverChange(int cost, int entered) throws
	CapacityExceededException, EmptyException, DisabledException {
		int changeDue = entered - cost;
		if(changeDue < 0) throw new
		InternalError("Cost was greater than entered, which should not happen");
		ArrayList<Integer> values = new ArrayList<Integer>();
		for(Integer ck : valueToIndexMap.keySet()) values.add(ck);
		Map<Integer, Object> map = changeHelper(values, 0, changeDue);
		List<Integer> res = ( List<Integer> ) map.get(changeDue);
		if(res == null) {
			Iterator<Integer> iter = map.keySet().iterator();
			Integer max = 0;
			while(iter.hasNext()) {
				Integer temp = iter.next();
				if(temp > max) max = temp;
			}
			res = ( List<Integer> ) map.get(max);
		}
		for(Integer ck : res) {
			CoinRack cr = vendingMachine.getCoinRack(ck);
			cr.releaseCoin();
			changeDue -= vendingMachine.getCoinKindForCoinRack(ck);
		}
		return changeDue;
	}
	/*** added by dVendingMachineLogic3
	 */
	@Override
	public void validCoinInserted(CoinSlot coinSlotSimulator, Coin coin) {
		availableFunds += coin.getValue().getValue();
	}
}