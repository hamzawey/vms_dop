package vm_experiement.product3.org.lsmr.vending.frontend;

/*** added by dCoin* modified by dCoin3
 */
public class Coin implements Deliverable {
	@Override
	public String toString() {
		return "" + getValue();
	}
	/*** added by dCoin3
	 */
	private Cents value;
	/*** added by dCoin3
	 */
	public Coin(Cents value) {
		if(value.compareTo(new Cents(0)) <= 0) throw new
		IllegalArgumentException("The value must be greater than 0: the argument passed was "
			+ value);
		this.value = value;
	}
	/*** added by dCoin3
	 */
	public Cents getValue() {
		return value;
	}
}