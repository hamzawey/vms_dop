package vm_experiement.product3.org.lsmr.vending.frontend;

/*** added by dCents
 */
public class Cents {
	private int quantity;
	public Cents(int quantity) {
		this.quantity = quantity;
	}
	@Override
	public int hashCode() {
		return quantity;
	}
	@Override
	public boolean equals(Object other) {
		if(other instanceof Cents) {
			Cents cents = ( Cents ) other;
			return quantity == cents.quantity;
		}
		return false;
	}
	public int getValue() {
		return quantity;
	}
	public int compareTo(Cents other) {
		return((quantity > other.quantity) ? 1 :((quantity == other.quantity) ? 0 :
				- 1));
	}
	public Cents add(Cents other) {
		quantity += other.quantity;
		return this;
	}
	public Cents subtract(Cents other) {
		quantity -= other.quantity;
		return this;
	}
	@Override
	public String toString() {
		return "" + quantity;
	}
}