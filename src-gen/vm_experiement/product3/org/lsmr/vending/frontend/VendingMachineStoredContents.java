package vm_experiement.product3.org.lsmr.vending.frontend;

import java.util.ArrayList;
import java.util.List;
/*** added by dVendingMachineStoredContents
 */
public class VendingMachineStoredContents {
	public List<Object> unusedCoinsForChange = new ArrayList<Object>();
	public List<Coin> paymentCoinsInStorageBin = new ArrayList<Coin>();
	public List<Object> unsoldPopCans = new ArrayList<Object>();
}