package vm_experiement.product3.org.lsmr.vending.frontend.hardware;

/*** added by dIndicatorLightListener
 */
public interface IndicatorLightListener extends AbstractHardwareListener {
	void activated(IndicatorLight light);
	void deactivated(IndicatorLight light);
}