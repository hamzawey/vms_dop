package vm_experiement.product3.org.lsmr.vending.frontend.hardware;

import vm_experiement.product3.org.lsmr.vending.frontend.Coin;
/*** added by dAbstractCoinAcceptor
 */
public interface AbstractCoinAcceptor {
	void acceptCoin(Coin coin) throws CapacityExceededException,
	DisabledException;
	boolean hasSpace();
}