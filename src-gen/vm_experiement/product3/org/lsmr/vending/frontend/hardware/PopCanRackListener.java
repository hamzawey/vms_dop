package vm_experiement.product3.org.lsmr.vending.frontend.hardware;

import vm_experiement.product3.org.lsmr.vending.frontend.PopCan;
/*** added by dPopCanRackListener
 */
public interface PopCanRackListener extends AbstractHardwareListener {
	void popCanAdded(PopCanRack popCanRack, PopCan popCan);
	void popCanRemoved(PopCanRack popCanRack, PopCan popCan);
	void popCansFull(PopCanRack popCanRack);
	void popCansEmpty(PopCanRack popCanRack);
	void popCansLoaded(PopCanRack rack, PopCan [] popCans);
	void popCansUnloaded(PopCanRack rack, PopCan [] popCans);
}