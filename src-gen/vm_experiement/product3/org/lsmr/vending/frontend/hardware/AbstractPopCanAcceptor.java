package vm_experiement.product3.org.lsmr.vending.frontend.hardware;

import vm_experiement.product3.org.lsmr.vending.frontend.PopCan;
/*** added by dAbstractPopCanAcceptor
 */
public interface AbstractPopCanAcceptor {
	public void acceptPopCan(PopCan popCan) throws CapacityExceededException,
	DisabledException;
}