package vm_experiement.product3.org.lsmr.vending.frontend.hardware;

/*** added by dSelectionButtonListener
 */
public interface SelectionButtonListener extends AbstractHardwareListener {
	void pressed(SelectionButton button);
}