package vm_experiement.product3.org.lsmr.vending.frontend.hardware;

import vm_experiement.product3.org.lsmr.vending.frontend.Coin;
/*** added by dCoinChannel
 */
public final class CoinChannel {
	private AbstractCoinAcceptor sink;
	public CoinChannel(AbstractCoinAcceptor sink) {
		this.sink = sink;
	}
	public void deliver(Coin coin) throws CapacityExceededException,
	DisabledException {
		getSink().acceptCoin(coin);
	}
	public boolean hasSpace() {
		return getSink().hasSpace();
	}
	public AbstractCoinAcceptor getSink() {
		return sink;
	}
}