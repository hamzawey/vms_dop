package vm_experiement.product3.org.lsmr.vending.frontend.hardware;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import vm_experiement.product3.org.lsmr.vending.frontend.PopCan;
/*** added by dPopCanRack
 */
public final class PopCanRack extends AbstractHardware<PopCanRackListener> {
	private int maxCapacity;
	private Queue<PopCan> queue = new LinkedList<PopCan>();
	private PopCanChannel sink;
	public PopCanRack(int capacity) {
		if(capacity <= 0) throw new
		SimulationException("Capacity cannot be non-positive: " + capacity);
		this.maxCapacity = capacity;
	}
	public int size() {
		return queue.size();
	}
	public int getCapacity() {
		return maxCapacity;
	}
	public void connect(PopCanChannel sink) {
		this.sink = sink;
	}
	public void addPopCan(PopCan popCan) throws CapacityExceededException,
	DisabledException {
		if(isDisabled()) throw new DisabledException();
		if(queue.size() >= maxCapacity) throw new CapacityExceededException();
		queue.add(popCan);
		notifyPopCanAdded(popCan);
		if(queue.size() >= maxCapacity) notifyPopCansFull();
	}
	public void dispensePopCan() throws DisabledException, EmptyException,
	CapacityExceededException {
		if(isDisabled()) throw new DisabledException();
		if(queue.isEmpty()) throw new EmptyException();
		PopCan popCan = queue.remove();
		notifyPopCanRemoved(popCan);
		if(sink == null) throw new
		SimulationException("The output channel is not connected");
		sink.acceptPopCan(popCan);
		if(queue.isEmpty()) notifyPopCansEmpty();
	}
	public void load(PopCan [] popCans) throws SimulationException {
		if(maxCapacity < queue.size() + popCans.length) throw new
		SimulationException("Capacity exceeded by attempt to load");
		for(PopCan popCan : popCans) queue.add(popCan);
		notifyLoad(popCans);
	}
	private void notifyLoad(PopCan [] popCans) {
		for(PopCanRackListener listener : listeners) listener.popCansLoaded(this,
			popCans);
	}
	public List<PopCan> unload() {
		List<PopCan> result = new ArrayList<PopCan>(queue);
		queue.clear();
		notifyUnload(result.toArray(new PopCan[result.size()]));
		return result;
	}
	private void notifyUnload(PopCan [] popCans) {
		for(PopCanRackListener listener : listeners) listener.popCansUnloaded(this,
			popCans);
	}
	private void notifyPopCanAdded(PopCan popCan) {
		for(PopCanRackListener listener : listeners) listener.popCanAdded(this,
			popCan);
	}
	private void notifyPopCansFull() {
		for(PopCanRackListener listener : listeners) listener.popCansFull(this);
	}
	private void notifyPopCansEmpty() {
		for(PopCanRackListener listener : listeners) listener.popCansEmpty(this);
	}
	private void notifyPopCanRemoved(PopCan popCan) {
		for(PopCanRackListener listener : listeners) listener.popCanRemoved(this,
			popCan);
	}
}