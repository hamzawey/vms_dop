package vm_experiement.product3.org.lsmr.vending.frontend.hardware;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import vm_experiement.product3.org.lsmr.vending.frontend.Coin;
/*** added by dCoinRack
 */
public final class CoinRack extends AbstractHardware<CoinRackListener>
implements AbstractCoinAcceptor {
	private int maxCapacity;
	private Queue<Coin> queue = new LinkedList<Coin>();
	private CoinChannel sink;
	public CoinRack(int capacity) {
		if(capacity <= 0) throw new
		SimulationException("Capacity must be positive: " + capacity);
		this.maxCapacity = capacity;
	}
	public int size() {
		return queue.size();
	}
	public void load(Coin [] coins) throws SimulationException {
		if(maxCapacity < queue.size() + coins.length) throw new
		SimulationException("Capacity of rack is exceeded by load");
		for(Coin coin : coins) queue.add(coin);
		notifyLoad(coins);
	}
	private void notifyLoad(Coin [] coins) {
		for(CoinRackListener listener : listeners) listener.coinsLoaded(this,
			coins);
	}
	public List<Coin> unload() {
		List<Coin> result = new ArrayList<Coin>(queue);
		queue.clear();
		notifyUnload(result.toArray(new Coin[result.size()]));
		return result;
	}
	private void notifyUnload(Coin [] coins) {
		for(CoinRackListener listener : listeners) listener.coinsUnloaded(this,
			coins);
	}
	public void connect(CoinChannel sink) {
		this.sink = sink;
	}
	public int getCapacity() {
		return maxCapacity;
	}
	@Override
	public void acceptCoin(Coin coin) throws CapacityExceededException,
	DisabledException {
		if(isDisabled()) throw new DisabledException();
		if(queue.size() >= maxCapacity) throw new CapacityExceededException();
		queue.add(coin);
		notifyCoinAdded(coin);
		if(queue.size() >= maxCapacity) notifyCoinsFull();
	}
	public void releaseCoin() throws CapacityExceededException, EmptyException,
	DisabledException {
		if(isDisabled()) throw new DisabledException();
		if(queue.size() == 0) throw new EmptyException();
		Coin coin = queue.remove();
		notifyCoinRemoved(coin);
		sink.deliver(coin);
		if(queue.isEmpty()) notifyCoinsEmpty();
	}
	@Override
	public boolean hasSpace() {
		return queue.size() < maxCapacity;
	}
	private void notifyCoinAdded(Coin coin) {
		for(CoinRackListener listener : listeners) listener.coinAdded(this, coin);
	}
	private void notifyCoinRemoved(Coin coin) {
		for(CoinRackListener listener : listeners) listener.coinRemoved(this, coin);
	}
	private void notifyCoinsFull() {
		for(CoinRackListener listener : listeners) listener.coinsFull(this);
	}
	private void notifyCoinsEmpty() {
		for(CoinRackListener listener : listeners) listener.coinsEmpty(this);
	}
}