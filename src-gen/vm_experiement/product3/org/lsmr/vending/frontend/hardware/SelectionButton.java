package vm_experiement.product3.org.lsmr.vending.frontend.hardware;

/*** added by dSelectionButton
 */
public final class SelectionButton extends
AbstractHardware<SelectionButtonListener> {
	public void press() {
		notifyPressed();
	}
	private void notifyPressed() {
		for(SelectionButtonListener listener : listeners) listener.pressed(this);
	}
}