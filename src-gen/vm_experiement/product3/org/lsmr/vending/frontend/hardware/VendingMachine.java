package vm_experiement.product3.org.lsmr.vending.frontend.hardware;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import vm_experiement.product3.org.lsmr.vending.frontend.Coin;
import vm_experiement.product3.org.lsmr.vending.frontend.PopCan;
import vm_experiement.product3.org.lsmr.vending.frontend.Cents;
/*** added by dVendingMachine_2* modified by dVendingMachine3
 */
public final class VendingMachine {
	private boolean safetyOn = false;
	private int [] coinKinds;
	private CoinSlot coinSlot;
	private CoinReceptacle receptacle, storageBin;
	private DeliveryChute deliveryChute;
	private CoinRack [] coinRacks;
	private Map<Integer, CoinChannel> coinRackChannels;
	private PopCanRack [] popCanRacks;
	private Display display;
	private SelectionButton [] buttons;
	private int [] popCanCosts;
	private String [] popCanNames;
	private IndicatorLight exactChangeLight, outOfOrderLight;
	public VendingMachine(int [] coinKinds, int selectionButtonCount, int
		coinRackCapacity, int popCanRackCapacity, int receptacleCapacity) {
		if(coinKinds == null) throw new
		SimulationException("Arguments may not be null");
		if(selectionButtonCount < 1 || coinRackCapacity < 1 || popCanRackCapacity <
			1) throw new SimulationException("Counts and capacities must be positive");
		if(coinKinds.length < 1) throw new
		SimulationException("At least one coin kind must be accepted");
		this.coinKinds = Arrays.copyOf(coinKinds, coinKinds.length);
		Set<Integer> currentCoinKinds = new HashSet<Integer>();
		for(int coinKind : coinKinds) {
			if(coinKind < 1) throw new
			SimulationException("Coin kinds must have positive values");
			if(currentCoinKinds.contains(coinKind)) throw new
			SimulationException("Coin kinds must have unique values");
			currentCoinKinds.add(coinKind);
		}
		display = new Display();
		coinSlot = new CoinSlot(coinKinds);
		receptacle = new CoinReceptacle(receptacleCapacity);
		storageBin = new CoinReceptacle(receptacleCapacity);
		deliveryChute = new DeliveryChute(receptacleCapacity);
		coinRacks = new CoinRack[coinKinds.length];
		coinRackChannels = new HashMap<Integer, CoinChannel>();
		for(int i = 0;
			i < coinKinds.length;
			i ++) {
			coinRacks[i] = new CoinRack(coinRackCapacity);
			coinRacks[i].connect(new CoinChannel(deliveryChute));
			coinRackChannels.put(new Integer(coinKinds[i]), new
				CoinChannel(coinRacks[i]));
		}
		popCanRacks = new PopCanRack[selectionButtonCount];
		for(int i = 0;
			i < selectionButtonCount;
			i ++) {
			popCanRacks[i] = new PopCanRack(popCanRackCapacity);
			popCanRacks[i].connect(new PopCanChannel(deliveryChute));
		}
		popCanNames = new String[selectionButtonCount];
		for(int i = 0;
			i < selectionButtonCount;
			i ++) popCanNames[i] = "";
		popCanCosts = new int[selectionButtonCount];
		for(int i = 0;
			i < selectionButtonCount;
			i ++) popCanCosts[i] = 1;
		buttons = new SelectionButton[selectionButtonCount];
		for(int i = 0;
			i < selectionButtonCount;
			i ++) buttons[i] = new SelectionButton();
		coinSlot.connect(new CoinChannel(receptacle), new
			CoinChannel(deliveryChute));
		receptacle.connect(coinRackChannels, new CoinChannel(deliveryChute), new
			CoinChannel(storageBin));
		exactChangeLight = new IndicatorLight();
		outOfOrderLight = new IndicatorLight();
	}
	public void configure(List<String> popCanNames, List<Integer> popCanCosts) {
		if(popCanNames.size() != this.popCanNames.length || popCanCosts.size() !=
			this.popCanCosts.length) throw new
		SimulationException("The number of names and costs must be identical to the number of pop can racks in the machine");
		for(String popName : popCanNames) if(popName.equals("")) throw new
		SimulationException("Pop can names cannot be the empty string");
		for(int popCost : popCanCosts) if(popCost < 1) throw new
		SimulationException("Pop can costs cannot be less than 1");
		popCanNames.toArray(this.popCanNames);
		int i = 0;
		for(Integer popCanCost : popCanCosts) this.popCanCosts[i ++] = popCanCost;
	}
	public void enableSafety() {
		safetyOn = true;
		coinSlot.disable();
		deliveryChute.disable();
		for(int i = 0;
			i < popCanRacks.length;
			i ++) popCanRacks[i].disable();
		for(int i = 0;
			i < coinRacks.length;
			i ++) coinRacks[i].disable();
		outOfOrderLight.activate();
	}
	public void disableSafety() {
		safetyOn = false;
		coinSlot.enable();
		deliveryChute.enable();
		for(int i = 0;
			i < popCanRacks.length;
			i ++) popCanRacks[i].enable();
		for(int i = 0;
			i < coinRacks.length;
			i ++) coinRacks[i].enable();
		outOfOrderLight.deactivate();
	}
	public boolean isSafetyEnabled() {
		return safetyOn;
	}
	public IndicatorLight getExactChangeLight() {
		return exactChangeLight;
	}
	public IndicatorLight getOutOfOrderLight() {
		return outOfOrderLight;
	}
	public SelectionButton getSelectionButton(int index) {
		return buttons[index];
	}
	public int getNumberOfSelectionButtons() {
		return buttons.length;
	}
	public CoinSlot getCoinSlot() {
		return coinSlot;
	}
	public CoinReceptacle getCoinReceptacle() {
		return receptacle;
	}
	public CoinReceptacle getStorageBin() {
		return storageBin;
	}
	public DeliveryChute getDeliveryChute() {
		return deliveryChute;
	}
	public int getNumberOfCoinRacks() {
		return coinRacks.length;
	}
	public CoinRack getCoinRack(int index) {
		return coinRacks[index];
	}
	public CoinRack getCoinRackForCoinKind(int kind) {
		CoinChannel cc = coinRackChannels.get(kind);
		if(cc != null) return ( CoinRack ) cc.getSink();
		return null;
	}
	public Integer getCoinKindForCoinRack(int index) {
		return coinKinds[index];
	}
	public int getNumberOfPopCanRacks() {
		return popCanRacks.length;
	}
	public String getPopKindName(int index) {
		return popCanNames[index];
	}
	public int getPopKindCost(int index) {
		return popCanCosts[index];
	}
	public PopCanRack getPopCanRack(int index) {
		return popCanRacks[index];
	}
	public Display getDisplay() {
		return display;
	}
	public void loadPopCans(int [] popCanCounts) {
		if(popCanCounts.length != getNumberOfPopCanRacks()) throw new
		SimulationException("Pop can counts have to equal number of racks");
		int i = 0;
		for(int popCanCount : popCanCounts) {
			if(popCanCount < 0) throw new
			SimulationException("Each count must not be negative");
			PopCanRack pcr = getPopCanRack(i);
			String name = getPopKindName(i);
			for(int pops = 0;
				pops < popCanCount;
				pops ++) {
				PopCan c = new PopCan(name);
				PopCan [] can = new PopCan[1];
				can[0] = c;
				pcr.load(can);
			}
			i ++;
		}
	}
	/*** added by dVendingMachine3
	 */
	public void loadCoins(int [] coinCounts) {
		if(coinCounts.length != getNumberOfCoinRacks()) throw new
		SimulationException("Coin counts have to equal number of racks");
		int i = 0;
		for(int coinCount : coinCounts) {
			if(coinCount < 0) throw new
			SimulationException("Each count must not be negative");
			CoinRack cr = getCoinRack(i);
			int value = getCoinKindForCoinRack(i);
			for(int coins = 0;
				coins < coinCount;
				coins ++) {
				Coin c = new Coin(new Cents(value));
				Coin [] can = new Coin[1];
				can[0] = c;
				cr.load(can);
			}
			i ++;
		}
	}
}