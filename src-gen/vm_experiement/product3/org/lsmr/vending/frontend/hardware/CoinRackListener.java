package vm_experiement.product3.org.lsmr.vending.frontend.hardware;

import vm_experiement.product3.org.lsmr.vending.frontend.Coin;
/*** added by dCoinRackListener
 */
public interface CoinRackListener extends AbstractHardwareListener {
	void coinsFull(CoinRack rack);
	void coinsEmpty(CoinRack rack);
	void coinAdded(CoinRack rack, Coin coin);
	void coinRemoved(CoinRack rack, Coin coin);
	void coinsLoaded(CoinRack rack, Coin [] coins);
	void coinsUnloaded(CoinRack rack, Coin [] coins);
}