package vm_experiement.product3.org.lsmr.vending.frontend.hardware;

/*** added by dDeliveryChuteListener
 */
public interface DeliveryChuteListener extends AbstractHardwareListener {
	void itemDelivered(DeliveryChute chute);
	void doorOpened(DeliveryChute chute);
	void doorClosed(DeliveryChute chute);
	void chuteFull(DeliveryChute chute);
}